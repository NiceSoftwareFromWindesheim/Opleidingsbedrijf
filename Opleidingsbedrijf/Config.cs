﻿using Microsoft.Extensions.Configuration;

namespace Opleidingsbedrijf
{
    public static class Config
    {
        public static IConfigurationRoot settings { get; set; }

        static Config()
        {
            var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
            settings = builder.Build();
        }
    }
}