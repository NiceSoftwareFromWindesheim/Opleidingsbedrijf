# Disciplina

## Database setup
Copy the `appsettings.example.json` to `appsettings.json`.

For people working inside a VM, change the IP to your Default Gateway(see `ipconfig` in terminal).

### Installing dotnet-ef
Ensure you have `dotnet` installed on your device(you can check this by simply running `dotnet` in a terminal).

In the command prompt navigate to the solution's folder eg.: `cd Documents/Opleidingsbedrijf` (Or a different path).
Run the following command: `dotnet tool install dotnet-ef`.

### Install docker
Either install Docker Desktop for Windows or for Mac. Or `docker` for Linux.

### Run MSSQL server
`docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Password123' -v ../mssql:/var/lib/mssql --restart always -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu`
If running on windows, use an absolute path like:
`docker run --name disiplina -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Password123" -v "//c/Users/Lars Oude Luttikhuis/Documents/GitHub/Opleidingsbedrijf/mssql:/var/lib/mssql" --restart always -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu`

If running on mac, use an absolute path like:
`docker run -e 'ACCEPT_EULA=Y' -e 'SA_PASSWORD=Password123' -v /Users/demiendrost/Documents/Opleidingsbedrijf/mssql:/var/lib/mssql --restart always -p 1433:1433 -d mcr.microsoft.com/mssql/server:2017-CU8-ubuntu`


## Migrations

### Creating a migration
`dotnet ef migrations add NameOfMigration` 

### Updating database
`dotnet ef database update`

## Knowledge base

### EntityFramework Core
- [Querying data](https://www.learnentityframeworkcore.com/dbset/querying-data)
