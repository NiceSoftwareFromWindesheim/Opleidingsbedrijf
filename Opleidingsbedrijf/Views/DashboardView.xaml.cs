using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.Extensions.Configuration;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf
{
    public partial class DashboardView : Page
    {
        public DashboardView()
        {
            InitializeComponent();
            FillCourseListBox();
            FillInvoiceListBox();

            RetrieveRegistrations();
        }

        private void FillCourseListBox()
        {
            var context = new MyContext();
            var courseinstances = context.CourseInstance;
            var courseinstance = courseinstances.ToList();
            PlannedCoursesGrid.ItemsSource = courseinstance;
        }

        private void FillInvoiceListBox()
        {
            var context = new MyContext();
            var invoices = context.Invoices.Where(i => i.PaymentDueOn <= DateTime.Now.AddDays(14) && !i.BeenPayedFor).ToList();

            UnpaidInvoicesDataGrid.ItemsSource = invoices;
        }

        private void RetrieveRegistrations()
        {
            var registrations = new List<dynamic>()
            {
                new
                {
                    Student = new
                    {
                        FirstName = "John",
                        LastName = "Smith",
                        Email = "john@smith.com",
                        Birth = DateTime.Parse("1965/5/20 08:00:00"),
                        Address = "Straat",
                        PostalCode = "0000AA",
                        HouseNumber = 1,
                        HouseNumberAddition = "",
                        City = "Stad",
                        PhoneNumber = "+316123456"
                    },
                    Course = new
                    {
                        Name = "Engels",
                        When = DateTime.Parse("2020/1/18 08:00:00")
                    }
                }
            };

            foreach (var registration in registrations)
            {
                Course course = Course.Find(registration.Course.Name);
                if (course == null)
                {
                    throw new Exception($"No course found with the name '{registration.Course.Name}'");
                }

                var courseInstance = course.CourseInstances.Find(cs => cs.StartDate == registration.Course.When);

                Student student = Student.Find(registration.Student.Email);
                if (student == null)
                {
                    student = Student.Add(
                        registration.Student.FirstName,
                        registration.Student.LastName,
                        registration.Student.Birth,
                        registration.Student.Email,
                        registration.Student.Address,
                        registration.Student.PostalCode,
                        registration.Student.HouseNumber,
                        registration.Student.HouseNumberAddition,
                        registration.Student.City,
                        registration.Student.PhoneNumber
                    );
                }

                if (!courseInstance.DetermineStudentRegistration(student))
                {
                    courseInstance.AddStudent(student);

                    try
                    {
                        var invoice = Invoice.GenerateInvoice(student, course, courseInstance);
                        var invoiceMail = invoice.GenerateInvoiceMail();
                        invoiceMail.Send();
                    }
                    catch (Exception exception)
                    {
                        Console.WriteLine(exception);
                    }
                }
            }
        }
    }
}