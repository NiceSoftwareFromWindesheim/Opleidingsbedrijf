﻿using System;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    public partial class ManageStudentControl : UserControl
    {
        public ManageStudentControl()
        {
            InitializeComponent();
            var student = StudentPresenter.SelectedStudent;
            FirstName.Text = student.FirstName;
            LastName.Text = student.LastName;
            Dob.SelectedDate = student.DateOfBirth;
            Email.Text = student.EmailAddress;
            Address.Text = student.Address;
            AddressZip.Text = student.PostalCode;
            AddressNumber.Text = student.HouseNumber.ToString();
            AddressNumberAddition.Text = student.HouseNumberAddition;
            AddressCity.Text = student.City;
            Telephone.Text = student.PhoneNumber;
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            var student = StudentPresenter.SelectedStudent;
            try
            {
                int id = StudentPresenter.SelectedStudent.Id;
                string firstname = FirstName.Text.ToString();
                string lastname = LastName.Text.ToString();
                DateTime dob = (DateTime)Dob.SelectedDate;
                string email = Email.Text.ToString();
                string address = Address.Text.ToString();
                string addresszip = AddressZip.Text.ToString();
                int addressnumber = int.Parse(AddressNumber.Text);
                string addressnumberaddition = AddressNumberAddition.Text.ToString();
                string addresscity = AddressCity.Text.ToString();
                string phonenumber = Telephone.Text.ToString();

                Student.Update(id, firstname, lastname, dob, email, address,addresszip, addressnumber, addressnumberaddition,addresscity,phonenumber);

                MessageBox.Show("Student Bijgewerkt.");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
         }
    }
}