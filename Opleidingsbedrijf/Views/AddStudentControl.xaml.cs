﻿using System;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.Views
{
    public partial class AddStudentControl : UserControl
    {
        public AddStudentControl()
        {
            InitializeComponent();
        }

        private void RegisterStudentButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                int houseNumber;
                int.TryParse(AddressNumber.Text, out houseNumber);

                Student.Add(
                    FirstName.Text,
                    LastName.Text,
                    DateTime.Parse(Dob.Text),
                    Email.Text,
                    Address.Text,
                    AddressZip.Text,
                    houseNumber,
                    AddressNumberAddition.Text,
                    AddressCity.Text,
                    Telephone.Text
                );

                MessageBox.Show($"Cursist {FirstName.Text} is aan gemaakt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}