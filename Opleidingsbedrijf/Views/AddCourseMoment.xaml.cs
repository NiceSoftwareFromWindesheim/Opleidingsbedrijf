﻿using System;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    public partial class AddCourseMoment : UserControl
    {
        public AddCourseMoment()
        {
            InitializeComponent();
        }

        private void SaveButton_OnClick(object sender, RoutedEventArgs e)
        {
            var CurrentCourse = AddCourseMomentPresenter.SelectedCourse;

            // Data ophalen
            var teacher = Teacher.Text;
            var location = Location.Text;

            var StartDate = DateTime.Parse(Date.Text);
            var hours = int.Parse(StartHour.Text);
            var minutes = int.Parse(StartMinutes.Text);
            StartDate = StartDate.AddHours(hours);
            StartDate = StartDate.AddMinutes(minutes);

            var EndDate = StartDate;
            var EndHours = int.Parse(DurationHour.Text);
            var EndMinutes = int.Parse(DurationMinutes.Text);
            EndDate = EndDate.AddHours(EndHours);
            EndDate = EndDate.AddMinutes(EndMinutes);

            //Data toevoegen
            CurrentCourse.AddInstance(
                teacher,
                location,
                StartDate,
                EndDate
                );
            
            MessageBox.Show("Cursusmoment is succesvol opgeslagen.", "Succes", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}