using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Microsoft.EntityFrameworkCore;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    /// <summary>
    ///     Interaction logic for CourseOverview.xaml
    /// </summary>
    public partial class CourseOverview : UserControl
    {
        public CourseOverview()
        {
            InitializeComponent();
            FillListBox();
            var context = new MyContext();
            var course = context.Courses.Include("CourseInstances.StudentCourseInstances").First();
            CoursePresenter.SelectedCourse = course;

            FillOverview(course);
        }

        private void SearchCourseTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var context = new MyContext();
            var item = SearchCourseTextBox.Text;

            CourseListBox.Items.Clear();
            if (item.Equals(""))
            {
                FillListBox();
            }
            else
            {
                var courses = context.Courses.Where(c => c.Name.ToLower().Contains(item));
                foreach (var course in courses) CourseListBox.Items.Add($"{course.Name}");
            }
        }

        private void CoursListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var context = new MyContext();
            var item = (string) CourseListBox.SelectedItem;

            if (item != null)
            {
                
                var course = context.Courses.Include("CourseInstances.StudentCourseInstances").FirstOrDefault(c => c.Name.ToLower().Contains(item.ToLower()));
                if (course != null)
                {
                    FillOverview(course);
                }
            }
        }

        private void FillListBox()
        {
            var context = new MyContext();
            var courses = context.Courses;
            foreach (var course in courses) CourseListBox.Items.Add($"{course.Name}");
        }
        private void FillOverview(Course course)
        {
            CourseNameLabel.Content = course.Name;
            CourseDiscriptionLabel.Content = course.Description;
            PriceTextbox.Text = course.Price.ToString();
            var noStudentsEnrolled = course.CourseInstances.Where(i => i.StartDate > DateTime.Now).Count(cI => cI.StudentCourseInstances.Count() != 0);
            TotalStudentsTextbox.Text = $"{noStudentsEnrolled}";
            var noStudentsFinished = course.CourseInstances.Where(i => i.StartDate < DateTime.Now).Count(cI => cI.StudentCourseInstances.Count() != 0);
            TotalStudentsFinishedTextbox.Text = $"{noStudentsFinished}";
            var noPlannedCourses = course.CourseInstances.Count(i => i.StartDate > DateTime.Now);
            PlannedDateTextbox.Text = $"{noPlannedCourses}";
            CoursePresenter.SelectedCourse = course;
        }
    }
}