﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    /// <summary>
    ///     Interaction logic for CourseData.xaml
    /// </summary>
    
    public class CheckedListItem
    {
        public CheckedListItem(int id, string name, bool isChecked)
        {
            Id = id;
            Name = name;
            IsChecked = isChecked;
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }
    
    public partial class CourseData : UserControl
    {
        public CourseInstance SelectedInstance { get; set; }
        
        public CourseData()
        {
            InitializeComponent();
            
            SelectedInstance = CoursePresenter.SelectedCourse.CourseInstances.FirstOrDefault();
            PopulateFields(SelectedInstance);

            var course = AddCourseMomentPresenter.SelectedCourse = CoursePresenter.SelectedCourse;
            
            PageTitle.Content = $"Cursus data overzicht | {course.Name}";

            if (course.CourseInstances != null)
                foreach (var instance in course.CourseInstances.Where(i => i.StartDate > DateTime.Now))
                    DataPlanned.Items.Add(instance.StartDate.ToString("d"));
        }

        private bool IsStudentInInstance(Student student)
        {
            return SelectedInstance.StudentCourseInstances?.Any(sI => sI.StudentId == student.Id) ?? false;
        }
        private void AddStudentsToSelector()
        {
            StudentSelector.Items.Clear();
            
            var context = new MyContext();
            var students = context.Students;
            foreach (var student in students)
            {
                if (student != null)
                {
                    StudentSelector.Items.Add(new CheckedListItem(
                        student.Id,
                        student.FullName,
                        IsStudentInInstance(student)
                    ));
                }
            }
        }

        private void PopulateFields(CourseInstance data)
        {
            if (data != null)
            {
                InstanceTitle.Content = data.StartDate.ToString("d");
                Time.Text = data.StartDate.TimeOfDay.ToString();
                Location.Text = data.Location;
                Teacher.Text = data.Teacher;
                Duration.Text = (data.EndDate.TimeOfDay - data.StartDate.TimeOfDay).ToString();
                MaxTotal.Text = CoursePresenter.SelectedCourse.MaxStudentCount.ToString();
                AddStudentsToSelector();
            }
        }
        
        private void DataPlannedListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var item = (string) DataPlanned.SelectedItem;
            if (item != null)
            {
                var res = CoursePresenter.SelectedCourse.CourseInstances.Find(
                    c => { return c.StartDate.ToString("d") == item; });
                SelectedInstance = res;
            }

            if (SelectedInstance != null)
            {
                PopulateFields(SelectedInstance);
            }
        }

        private void Save_OnClick(object sender, RoutedEventArgs e)
        {
            var context = new MyContext();
            var students = context.Students;

            // Get list values;
            var studentsList = StudentSelector.Items;
            
            // Parse list to students
            var listAsStudents = new List<Student>();
            foreach (var student in studentsList)
            {
                // Check if student checkbox is checked and if so add it to the list
                CheckedListItem st = (CheckedListItem) student;
                if (st.IsChecked)
                {
                    var studentInQuestion = students.FirstOrDefault(s => s.Id == st.Id);
                    listAsStudents.Add(studentInQuestion);

                    // Send mail in case the student is not yet registered to the course
                    if (!SelectedInstance.DetermineStudentRegistration(studentInQuestion))
                    {
                        try
                        {
                            var invoice = Invoice.GenerateInvoice(studentInQuestion,
                                AddCourseMomentPresenter.SelectedCourse, SelectedInstance);
                            var invoiceMail = invoice.GenerateInvoiceMail();
                            invoiceMail.Send();
                        }
                        catch (Exception exception)
                        {
                            Console.WriteLine(exception);
                            MessageBox.Show("Mailen van de cursist is niet gelukt", "FOUTCODE", MessageBoxButton.OK,
                                MessageBoxImage.Error);
                        }
                    }

                }
            }

            // Check if the number of students exceeds the maximum amount.
            if (listAsStudents.Count > AddCourseMomentPresenter.SelectedCourse.MaxStudentCount)
            {
                MessageBox.Show(
                    $"Er zijn meer dan {AddCourseMomentPresenter.SelectedCourse.MaxStudentCount} cursisten geselecteerd. Meld er {listAsStudents.Count - AddCourseMomentPresenter.SelectedCourse.MaxStudentCount} af om op te slaan.",
                    "Te veel studenten", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            
            // Try updating the database accordingly
            try
            {
                SelectedInstance.ReplaceStudents(listAsStudents);
                MessageBox.Show("Cursisten zijn succesvol bijgewerkt.", "Succes", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
                MessageBox.Show("Cursus of cursusmoment niet gevonden.", "FOUTCODE", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void SearchStudentData_TextChanged(object sender, TextChangedEventArgs e)
        {
            StudentSelector.Items.Clear();

            var item = SearchStudentData.Text;
            var context = new MyContext();

            if (item.Equals(""))
            {
                var students = context.Students;
                foreach (var student in students)
                {
                    if (student != null)
                    {
                        StudentSelector.Items.Add(new CheckedListItem(
                            student.Id,
                            student.FullName,
                            IsStudentInInstance(student)
                        ));
                    }
                }
            }
            else
            {
                var students = context.Students.Where(s =>
                    s.FirstName.ToLower().Contains(item.ToLower()) ||
                    s.LastName.ToLower().Contains(item.ToLower()));
                foreach (var student in students)
                {
                    if (student != null)
                    {
                        StudentSelector.Items.Add(new CheckedListItem(
                            student.Id,
                            student.FullName,
                            IsStudentInInstance(student)
                        ));
                    }
                }
            }
        }
    }
}