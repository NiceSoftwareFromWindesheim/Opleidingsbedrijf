using System.Linq;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    public partial class StudentOverview
    {
        public StudentOverview()
        {
            InitializeComponent();
            FillListBox();
            var context = new MyContext();
            Student student = context.Students.First();
            FillOverview(student);
        }

        private void StudentListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var context = new MyContext();
            var item = (string) StudentListBox.SelectedItem;

            if (item != null)
            {
                var student =
                    context.Students.FirstOrDefault(s => item.Contains(s.FirstName) && item.Contains(s.LastName));
                if (student != null)
                {
                    FillOverview(student);
                }
            }
        }

        private void FillOverview(Student student)
        {
            StudentPresenter.SelectedStudent = student;
            StudentIdLabel.Text = student.Id.ToString();
            StudentFirstNameLabel.Text = student.FirstName;
            StudentLastNameLabel.Text = student.LastName;
            StudentDoBLabel.Text = student.DateOfBirth.ToString("d");
            StudentEmailLabel.Text = student.EmailAddress;
            StudentPostalCodeLabel.Text = student.PostalCode;
            StudentAddressLabel.Text = student.Address;
            StudentHouseNumberLabel.Text = $"{student.HouseNumber}{student.HouseNumberAddition}";
            StudentCityLabel.Text = student.City;
            StudentPhoneNumberLabel.Text = student.PhoneNumber;
        }

        private void FillListBox()
        {
            var context = new MyContext();
            var students = context.Students;
            foreach (var student in students) StudentListBox.Items.Add(student.FullName);
        }

        private void SearchStudentTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            var context = new MyContext();

            StudentListBox.Items.Clear();
            if (SearchStudentTextBox.Text.Equals(""))
            {
                FillListBox();
            }
            else
            {
                var students = context.Students.Where(s =>
                    s.FirstName.ToLower().Contains(SearchStudentTextBox.Text.ToLower()) ||
                    s.LastName.ToLower().Contains(SearchStudentTextBox.Text.ToLower()));
                foreach (var item in students) StudentListBox.Items.Add(item.FullName);
            }
        }
    }
}