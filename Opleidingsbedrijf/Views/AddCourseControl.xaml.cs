﻿using System;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    public partial class AddCourseControl : UserControl
    {
        public AddCourseControl()
        {
            InitializeComponent();
        }

        private void ClearInput()
        {
            Name.Text = "";
            Price.Text = "0.00";
            Discription.Text = "";
            MaxStudents.Text = "";
            Requirements.Text = "";
        }
        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var name = Name.Text;
                double price;
                var priceDouble = Double.TryParse(Price.Text, out price);
                var description = Discription.Text;
                var maxStudentCount = int.Parse(MaxStudents.Text);
                var requirements = Requirements.Text;
                if (
                    priceDouble &&
                    price > 0 &&
                    name.Length >= 5 &&
                    description.Length > 10 &&
                    maxStudentCount > 1
                )
                {
                    Course.Add(name, description, price, maxStudentCount, requirements);
                    MessageBox.Show("Cursus is aangemaakt.");
                    ClearInput();
                }
                else
                {
                    MessageBox.Show("Niet alle waarden zijn goed ingevuld.", "Faulty input", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}