using System;
using System.Printing;
using System.Windows;
using System.Windows.Controls;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace Opleidingsbedrijf.Views
{
    /// <summary>
    ///     Interaction logic for ManageCourseControl.xaml
    /// </summary>
    public partial class ManageCourseControl : UserControl
    {
        public ManageCourseControl()
        {
            InitializeComponent();
            Name.Text = CoursePresenter.SelectedCourse.Name;
            Discription.Text = CoursePresenter.SelectedCourse.Description;
            Price.Text = CoursePresenter.SelectedCourse.Price.ToString();
            MaxStudents.Text = CoursePresenter.SelectedCourse.MaxStudentCount.ToString();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                var id = CoursePresenter.SelectedCourse.Id;
                var name = Name.Text;
                double price;
                var priceDouble = Double.TryParse(Price.Text, out price);
                var description = Discription.Text;
                var maxStudentCount = int.Parse(MaxStudents.Text);
                var requirements = Requirements.Text;
                if (priceDouble)
                {
                    Course.Update(id, name, price, description, maxStudentCount, requirements);
                    MessageBox.Show("Cursus is aangepast.");
                }
                else
                {
                    MessageBox.Show("Prijs is geen getal.", "Faulty input", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }
    }
}