﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.Views
{
	public partial class InvoiceOverview : UserControl
	{
		public Invoice SelectedInvoice { get; set; }
		public InvoiceOverview()
		{
			InitializeComponent();
			FillListBox();
			RadioButtonAll.IsChecked = true;
		}
		
		private void InvoiceListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (InvoiceListBox.SelectedItem != null)
			{
				var item = (int)InvoiceListBox.SelectedItem;

				if (item != null)
				{
					var invoice = Invoice.Find(item);
					if (invoice != null)
					{
						SelectedInvoice = invoice;
						InvoiceStudentInvoiceID.Text = invoice.Id.ToString();
						InvoiceStudentName.Text = invoice.Student.FullName;
						InvoiceStudentCourses.Text = invoice.Course.Name;
						InvoiceStudentAmount.Text = "€" + invoice.Amount.ToString("F");
						InvoiceStudentPaymentDueOn.Text = invoice.PaymentDueOn.ToString("d");
						if (invoice.BeenPayedFor)
						{
							RadioButtonPaid.IsChecked = true;
							RadioButtonUnPaid.IsChecked = false;
						}
						else
						{
							RadioButtonUnPaid.IsChecked = true;
							RadioButtonPaid.IsChecked = false;
						}

					}
				}
			}
		}
		
		private void FillListBox()
		{
			var context = new MyContext();
			var invoices = context.Invoices;
			foreach (var invoice in invoices) InvoiceListBox.Items.Add(invoice.Id);
		}
		
		private void SearchInvoiceTextBox_TextChanged(object sender, TextChangedEventArgs e)
		{
			var context = new MyContext();
			var item = SearchInvoiceTextBox.Text;

			InvoiceListBox.Items.Clear();
			if (SearchInvoiceTextBox.Text.Equals(""))
			{
				FillListBox();
			}
			else
			{
				var invoices = context.Invoices.Where(i => i.Id.ToString().Contains(item));
				foreach (var invoice in invoices) InvoiceListBox.Items.Add(invoice.Id);
			}
		}

		private void RadioButton_Checked(object sender, RoutedEventArgs e)
		{
			InvoiceListBox.UnselectAll();
			ICollection<Invoice> invoices;
			InvoiceListBox.Items.Clear();

			if (RadioButtonAll.IsChecked == true)
			{
				var context = new MyContext();
				invoices = context.Invoices.ToList();
			}
			else if (RadioButtonExpired.IsChecked == true)
			{
				invoices = Invoice.FindExpired();
			}
			else if (RadioButtonSatisfied.IsChecked == true)
			{
				invoices = Invoice.FindBeenPayedFor(true);
			}
			else
			{
				invoices = Invoice.FindBeenPayedFor(false);
			}
			foreach (var invoice in invoices) InvoiceListBox.Items.Add(invoice.Id);
		}

		private void SaveButton_Click(object sender, RoutedEventArgs e)
		{
			var paid = RadioButtonPaid.IsChecked == true;

			var id = int.Parse(InvoiceStudentInvoiceID.Text);
			var amount = double.Parse(InvoiceStudentAmount.Text.Substring(1));
			var paymentDueOn = DateTime.Parse(InvoiceStudentPaymentDueOn.Text);

			Invoice.Update(id, amount, paymentDueOn, paid);
		}

		private void Remind_Button_Click(object sender, RoutedEventArgs e)
		{
			MessageBox.Show(SelectedInvoice.GenerateInvoiceMail(true).Send()
				? "Herinnering is verzonden."
				: "Herinnering versturen is mislukt.");
		}
	}
}
