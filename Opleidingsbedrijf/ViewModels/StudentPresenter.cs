﻿using System.ComponentModel;
using System.Windows.Input;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.ViewModels
{
    public class StudentPresenter : Presenter
    {
        private object _selectedContent;

        public StudentPresenter()
        {
            // Set default content
            SelectedContent = new StudentOverviewModel();
        }

        public static Student SelectedStudent { get; internal set; }

        public Course SelectedCourse { get; internal set; }

        public sealed override object SelectedContent
        {
            get => _selectedContent;
            set
            {
                _selectedContent = value;
                OnPropertyChanged("SelectedContent");
            }
        }

        public ICommand LoadEditContentCommand => new DelegateCommand(ExecuteLoadEditContent);

        public ICommand LoadAddContentCommand => new DelegateCommand(ExecuteAddLoadContent);
        public override event PropertyChangedEventHandler PropertyChanged;


        public void ExecuteAddLoadContent()
        {
            if (SelectedContent is StudentAddModel)
                SelectedContent = new StudentOverviewModel();
            else
                SelectedContent = new StudentAddModel();
        }

        public void ExecuteLoadEditContent()
        {
            if (SelectedContent is StudentManageModel)
                SelectedContent = new StudentOverviewModel();
            else
                SelectedContent = new StudentManageModel();
        }

        public override void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}