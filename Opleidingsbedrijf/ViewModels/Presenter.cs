﻿using System.ComponentModel;
using System.Windows.Input;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.ViewModels
{
    public abstract class Presenter : INotifyPropertyChanged
    {
        private object _selectedContent;
        public abstract object SelectedContent { get; set; }
        public abstract event PropertyChangedEventHandler PropertyChanged;
        public abstract void OnPropertyChanged(string name);
    }
}