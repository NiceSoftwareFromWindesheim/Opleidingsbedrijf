﻿using System.ComponentModel;
using System.Windows.Input;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.ViewModels
{
    public class CoursePresenter : Presenter
    {
        private object _selectedContent;

        public CoursePresenter()
        {
            // Set default content
            SelectedContent = new CourseOverviewModel();
        }

        public static Course SelectedCourse { get; internal set; }

        public sealed override object SelectedContent
        {
            get => _selectedContent;
            set
            {
                _selectedContent = value;
                OnPropertyChanged("SelectedContent");
            }
        }

        public ICommand LoadContentCommand => new DelegateCommand(ExecuteLoadContent);
        public ICommand LoadDateContentCommand => new DelegateCommand(ExecuteLoadDataContent);
        public ICommand LoadAddCommand => new DelegateCommand(ExecuteLoadAddContent);
        public override event PropertyChangedEventHandler PropertyChanged;

        public void ExecuteLoadContent()
        {
            if (SelectedContent is CourseManageModel)
                SelectedContent = new CourseOverviewModel();
            else
                SelectedContent = new CourseManageModel();
        }

        public void ExecuteLoadDataContent()
        {
            if (SelectedContent is CourseDataModel)
                SelectedContent = new CourseOverviewModel();
            else
                SelectedContent = new CourseDataModel();
        }
        
        public void ExecuteLoadAddContent()
        {
            if (SelectedContent is CourseAddModel)
                SelectedContent = new CourseOverviewModel();
            else
                SelectedContent = new CourseAddModel();
        }

        public override void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}