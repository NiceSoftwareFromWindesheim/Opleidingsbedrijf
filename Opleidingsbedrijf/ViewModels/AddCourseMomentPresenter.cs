﻿using System.ComponentModel;
using System.Windows.Input;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.ViewModels
{
    public class AddCourseMomentPresenter : Presenter
    {
        public static Course SelectedCourse;
        
        private object _selectedContent;

        public AddCourseMomentPresenter()
        {
            // Set default content
            SelectedContent = new CourseDataModel();
        }

        public sealed override object SelectedContent
        {
            get => _selectedContent;
            set
            {
                _selectedContent = value;
                OnPropertyChanged("SelectedContent");
            }
        }

        public ICommand LoadAddInstanceContentCommand => new DelegateCommand(ExecuteLoadContent);
        public override event PropertyChangedEventHandler PropertyChanged;

        public void ExecuteLoadContent()
        {
            if (SelectedContent is AddCourseMomentModel)
                SelectedContent = new CourseDataModel();
            else
                SelectedContent = new AddCourseMomentModel();
        }

        public override void OnPropertyChanged(string name)
        {
            var handler = PropertyChanged;
            handler?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
