﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Opleidingsbedrijf.Migrations
{
    public partial class InvoicePayedFor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "BeenPayedFor",
                table: "Invoices",
                nullable: false,
                defaultValue: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BeenPayedFor",
                table: "Invoices");
        }
    }
}
