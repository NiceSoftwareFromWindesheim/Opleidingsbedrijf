﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Opleidingsbedrijf.Models;

namespace Opleidingsbedrijf.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                "Courses",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    MaxStudentCount = table.Column<int>(),
                    Requirements = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Courses", x => x.Id); });

            migrationBuilder.CreateTable(
                "Students",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    DateOfBirth = table.Column<DateTime>(),
                    EmailAddress = table.Column<string>(nullable: true),
                    PostalCode = table.Column<string>(nullable: true),
                    HouseNumber = table.Column<int>(),
                    HouseNumberAddition = table.Column<string>(nullable: true),
                    City = table.Column<string>(nullable: true),
                    PhoneNumber = table.Column<string>(nullable: true)
                },
                constraints: table => { table.PrimaryKey("PK_Students", x => x.Id); });

            migrationBuilder.CreateTable(
                "CourseInstance",
                table => new
                {
                    Id = table.Column<int>()
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Teacher = table.Column<string>(nullable: true),
                    StartDate = table.Column<DateTime>(),
                    EndDate = table.Column<DateTime>(),
                    CourseId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CourseInstance", x => x.Id);
                    table.ForeignKey(
                        "FK_CourseInstance_Courses_CourseId",
                        x => x.CourseId,
                        "Courses",
                        "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                "StudentCourseInstance",
                table => new
                {
                    CourseInstanceId = table.Column<int>(),
                    StudentId = table.Column<int>()
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourseInstance", x => new {x.StudentId, x.CourseInstanceId});
                    table.ForeignKey(
                        "FK_StudentCourseInstance_CourseInstance_CourseInstanceId",
                        x => x.CourseInstanceId,
                        "CourseInstance",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        "FK_StudentCourseInstance_Students_StudentId",
                        x => x.StudentId,
                        "Students",
                        "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                "IX_CourseInstance_CourseId",
                "CourseInstance",
                "CourseId");

            migrationBuilder.CreateIndex(
                "IX_StudentCourseInstance_CourseInstanceId",
                "StudentCourseInstance",
                "CourseInstanceId");

            var jongLeren = migrationBuilder.InsertData(
                "Courses",
                new[]
                {
                    "Name", "Description", "MaxStudentCount",
                    "Requirements"
                },
                new object[]
                {
                    "Jongleren", "Leer het jong, jongleren", 15,
                    "Deze cursus behoeft geen kennis en vaardigheid, voor ieder met twee handen en voeten is deze geschikt"
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent1, DateTime.Parse("2020/1/14 08:00:00"), DateTime.Parse("2020/1/14 16:30:00"), 1
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent1, DateTime.Parse("2020/1/15 08:00:00"), DateTime.Parse("2020/1/15 16:30:00"), 1
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent1, DateTime.Parse("2020/1/16 08:00:00"), DateTime.Parse("2020/1/16 16:30:00"), 1
                });

            migrationBuilder.InsertData(
                "Courses",
                new[]
                {
                    "Name", "Description", "MaxStudentCount",
                    "Requirements"
                },
                new object[]
                {
                    "Engels", "Leer op professionele manier Engels te spreken en schrijven.", 25,
                    "Basiskennis van de Engelse taal is een vereiste."
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent2, DateTime.Parse("2020/1/17 08:00:00"), DateTime.Parse("2020/1/17 16:30:00"), 2
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent2, DateTime.Parse("2020/1/18 08:00:00"), DateTime.Parse("2020/1/18 16:30:00"), 2
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent2, DateTime.Parse("2020/1/19 08:00:00"), DateTime.Parse("2020/1/19 16:30:00"), 2
                });


            migrationBuilder.InsertData(
                "Courses",
                new[]
                {
                    "Name", "Description", "MaxStudentCount",
                    "Requirements"
                },
                new object[]
                {
                    "Programmeren met C#", "Leer de beginselen van het object georienteerd programmeren met C#.", 25,
                    "Basiskennis van de Engelse taal is een vereiste."
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent3, DateTime.Parse("2020/1/18 08:00:00"), DateTime.Parse("2020/1/19 16:30:00"), 3
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent3, DateTime.Parse("2020/1/19 08:00:00"), DateTime.Parse("2020/1/19 16:30:00"), 3
                });

            migrationBuilder.InsertData(
                "CourseInstance",
                new[]
                {
                    "Teacher", "StartDate", "EndDate", "CourseId"
                },
                new object[]
                {
                    DocentC.Docent3, DateTime.Parse("2020/1/20 08:00:00"), DateTime.Parse("2020/1/20 16:30:00"), 3
                });

            migrationBuilder.InsertData(
                "Students",
                new[]
                {
                    "FirstName", "LastName", "DateOfBirth",
                    "EmailAddress", "PostalCode", "HouseNumber",
                    "HouseNumberAddition", "City", "PhoneNumber"
                },
                new object[]
                {
                    "Jan", "Jansen", DateTime.Parse("1966/1/17 08:00:00"),
                    "JanJansen@diseplinia.nl", "8416XX",
                    45, "B", "Den Haag", "0642596354"
                });

            migrationBuilder.InsertData(
                "Students",
                new[]
                {
                    "FirstName", "LastName", "DateOfBirth",
                    "EmailAddress", "PostalCode", "HouseNumber",
                    "HouseNumberAddition", "City", "PhoneNumber"
                },
                new object[]
                {
                    "Piet", "Pietersen", DateTime.Parse("1965/5/20 08:00:00"),
                    "PietPietersen@diseplinia.nl", "8416XX", 45,
                    "B", "Den Haag", "0642596354"
                });

            migrationBuilder.InsertData(
                "Students",
                new[]
                {
                    "FirstName", "LastName", "DateOfBirth",
                    "EmailAddress", "PostalCode", "HouseNumber",
                    "HouseNumberAddition", "City", "PhoneNumber"
                },
                new object[]
                {
                    "Klaas", "Klaasen", DateTime.Parse("1978/3/25 08:00:00"),
                    "KlaasKlaasen@diseplinia.nl", "8416XX", 45,
                    "B", "Den Haag", "0642596354"
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                "StudentCourseInstance");

            migrationBuilder.DropTable(
                "CourseInstance");

            migrationBuilder.DropTable(
                "Students");

            migrationBuilder.DropTable(
                "Courses");
        }
    }
}