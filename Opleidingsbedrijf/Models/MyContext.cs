﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Opleidingsbedrijf.Models
{
    public class MyContext : DbContext
    {
        public DbSet<Student> Students { get; set; }
        public DbSet<Course> Courses { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<CourseInstance> CourseInstance { get; set; }

        public MyContext()
        {
        }

        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                var builder = new ConfigurationBuilder().AddJsonFile("appsettings.json");
                var config = builder.Build();
                optionsBuilder.UseSqlServer(config.GetConnectionString("DisciplinaDatabase"));
            }

            optionsBuilder
                // Using lazy loading so we dont have to explicit calls to thse database ourselves.
                .UseLazyLoadingProxies();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Because EF Core 3 does not automatically resolves join tables and entities we have to do it ourselves.
            modelBuilder.Entity<StudentCourseInstance>().HasKey(sc => new {sc.StudentId, sc.CourseInstanceId});
        }
    }
}