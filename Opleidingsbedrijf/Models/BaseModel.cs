﻿namespace Opleidingsbedrijf.Models
{
    public class BaseModel
    {
        private static MyContext _context;

        public static MyContext Context
        {
            get
            {
                if (_context == null)
                {
                    _context = new MyContext();
                }

                return _context;
            }
            set { _context = value; }
        }
    }
}