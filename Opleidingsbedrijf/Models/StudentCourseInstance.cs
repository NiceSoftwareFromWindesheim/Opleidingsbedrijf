﻿namespace Opleidingsbedrijf.Models
{
    public class StudentCourseInstance
    {
        public int CourseInstanceId { get; set; }
        public virtual CourseInstance CourseInstance { get; set; }
        public int StudentId { get; set; }
        public virtual Student Student { get; set; }
    }
}