﻿namespace Opleidingsbedrijf.Models
{
    public class CourseOverviewModel
    {
        public int id { get; set; }

        public override string ToString()
        {
            return "CourseOverviewModel";
        }
    }
}