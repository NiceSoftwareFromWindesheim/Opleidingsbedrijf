﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace Opleidingsbedrijf.Models
{
    public class Student : BaseModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string EmailAddress { get; set; }
        public string Address { get; set; }
        public string FullName => FirstName + " " + LastName;
        public string PostalCode { get; set; }
        public int HouseNumber { get; set; }
        public string HouseNumberAddition { get; set; }
        public string City { get; set; }
        public string PhoneNumber { get; set; }

        public virtual ICollection<StudentCourseInstance> StudentCourseInstances { get; set; }

        public static Student Add(string firstName, string lastName, DateTime dateOfBirth, string emailAddress,
            string address, string postalCode, int houseNumber, string houseNumberAddition, string city, string phoneNumber)
        {
            var context = Context;
            var student = new Student
            {
                FirstName = firstName,
                LastName = lastName,
                DateOfBirth = dateOfBirth,
                EmailAddress = emailAddress,
                Address = address,
                PostalCode = postalCode,
                HouseNumber = houseNumber,
                HouseNumberAddition = houseNumberAddition,
                City = city,
                PhoneNumber = phoneNumber
            };

            context.Add(student);
            context.SaveChanges();
            return student;
        }

        public static Student Update(int id, string firstName, string lastName, DateTime dateOfBirth, string emailAddress, string address,
            string postalCode, int houseNumber, string houseNumberAddition, string city, string phonenumber)
        {
            var context = Student.Context;
            var student = context.Students.First(c => c.Id == id);
            {
                              student.FirstName = firstName;
                              student.LastName = lastName;
                              student.DateOfBirth = dateOfBirth;
                              student.EmailAddress = emailAddress;
                              student.Address = address;
                              student.PostalCode = postalCode;
                              student.HouseNumber = houseNumber;
                              student.HouseNumberAddition = houseNumberAddition;
                              student.City = city;
                              student.PhoneNumber = phonenumber;
            };

            context.Students.Update(student);
            // Save changes in database
            context.SaveChanges();

            return student;
        }

        public static Student Find(string emailAdress)
        {
            var context = new MyContext();
            try
            {
                return context.Students.First(s => s.EmailAddress == emailAdress);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}