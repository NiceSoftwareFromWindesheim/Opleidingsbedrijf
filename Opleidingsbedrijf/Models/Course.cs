using System;
using System.Collections.Generic;
using System.Linq;

namespace Opleidingsbedrijf.Models
{
    public static class DocentC
    {
        public const string
            Docent1 = "Joop knoop",
            Docent2 = "Piet Hogebiet",
            Docent3 = "Klaas Lokaas",
            Docent4 = "Hans Kadans";
    }
    
    public static class LocationC
    {
        public const string
            Location1 = "Gebouw X",
            Location2 = "Gebouw T",
            Location3 = "Gebouw B", 
            Location4 = "Gebouw A";
    }

    public class Course : BaseModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double Price { get; set; }
        public int MaxStudentCount { get; set; }
        public virtual List<CourseInstance> CourseInstances { get; set; }
        public string Requirements { get; set; }
        public static Course Add(string name, string description, double price, int maxStudentCount, string requirements = "NotImplemented")
        {
            var context = Context;
            var courseToAdd = new Course
            {
                Name = name,
                Description = description,
                Price = price,
                MaxStudentCount = maxStudentCount,
                Requirements = requirements
            };

            context.Add(courseToAdd);
            context.SaveChanges();
            return courseToAdd;
        }
        public static Course Update(int id, string name, double price, string description, int maxStudentCount, string requirements)
        {
            var context = Context;
            var course = context.Courses.First(c => c.Id == id);

            if (course != null)
            {
                course.Name = name;
                course.Price = price;
                course.Description = description;
                course.MaxStudentCount = maxStudentCount;
                course.Requirements = requirements;
                
            }
            context.Courses.Update(course);
            context.SaveChanges();
            return course;
        }

        public Course AddInstance(string teacher, string location, DateTime startDate, DateTime endDate)
        {
            var context = new MyContext();

            var newInstance = new CourseInstance() {
                Course = this,
                Teacher = teacher,
                Location = location,
                StartDate = startDate,
                EndDate = endDate
            };
            
            this.CourseInstances.Add(newInstance);
            context.Courses.Update(this);
            // Save changes in database
            context.SaveChanges();
            return this;
        }
        
        public static Course Find(string courseName)
        {
            var context = new MyContext();
            try
            {
                return context.Courses.First(s => s.Name == courseName);
            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}