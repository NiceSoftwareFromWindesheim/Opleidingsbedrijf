﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using Castle.DynamicProxy.Generators;
using Microsoft.EntityFrameworkCore;

namespace Opleidingsbedrijf.Models
{
    public class CourseInstance
    {
        public int Id { get; set; }
        public virtual ICollection<StudentCourseInstance> StudentCourseInstances { get; set; }
        public string Teacher { get; set; }
        public string Location { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public virtual Course Course { get; set; }

        public void AddStudent(Student student)
        {
            var context = new MyContext();
            this.StudentCourseInstances.Add( new StudentCourseInstance
            {
                Student = student,
                CourseInstance = this
            });
            // Save changes in database
            context.CourseInstance.Update(this);
            context.SaveChanges();
        }

        public CourseInstance Add(Course course, string teacher, string location, DateTime startDate, DateTime endDate)
        {
            var context = new MyContext();
            var instance = new CourseInstance
            {
                Course = course,
                Teacher = teacher,
                Location = location,
                StartDate = startDate,
                EndDate = endDate
            };

            context.Add(instance);
            context.SaveChanges();
            return instance;
        }

        public void ReplaceStudents(List<Student> listAsStudents)
        {
            //Select instance
            var context = new MyContext();
            var instance = context
                .Courses.FirstOrDefault(c => c.Id == this.Course.Id)
                ?.CourseInstances.FirstOrDefault(i => i.Id == this.Id);
            if (instance == null)
            {
                throw new NullReferenceException();
            }
            
            instance.StudentCourseInstances.Clear();
            foreach (var student in listAsStudents)
            {
                instance.StudentCourseInstances.Add( new StudentCourseInstance
                {
                    Student = student,
                    CourseInstance = this
                });
            }
            
            StudentCourseInstances = instance.StudentCourseInstances;
            context.SaveChanges();
        }

        public bool DetermineStudentRegistration(Student student)
        {
            //Select instance
            var context = new MyContext();
            var instance = context
                .Courses.FirstOrDefault(c => c.Id == this.Course.Id)
                ?.CourseInstances.FirstOrDefault(i => i.Id == this.Id);
            if (instance == null)
            {
                throw new NullReferenceException();
            }

            return instance.StudentCourseInstances.Any(c => c.StudentId == student.Id);
        }
    }
}