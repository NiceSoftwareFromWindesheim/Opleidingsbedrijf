﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Opleidingsbedrijf.Models
{
	public class Invoice
	{
		public int Id { get; set; }
		public virtual Student Student { get; set; }
		public virtual Course Course { get; set; }
		public double Amount { get; set; }
		public DateTime PaymentDueOn { get; set; }
		
		public bool BeenPayedFor { get; set; }

		public static Invoice Add(Student student, Course course, double amount, DateTime paymentDueOn, bool beenPayedFor = false)
		{
			var context = new MyContext();

			context.Entry(student).State = EntityState.Modified;
			context.Entry(course).State = EntityState.Modified;
			
			var invoice = new Invoice()
			{
				Student = student,
				Course = course,
				Amount = amount,
				PaymentDueOn = paymentDueOn,
				BeenPayedFor = beenPayedFor
			};

			context.Add(invoice);
			context.SaveChanges();
			return invoice;
		}
		public static void Update(int id, double amount, DateTime paymentDueOn, bool beenPayedFor) 
		{
			var context = new MyContext();
			var invoice = context.Invoices.First(i => i.Id == id);
			
			if(invoice != null)
			{
				invoice.Id = id;
				invoice.Amount = amount;
				invoice.PaymentDueOn = paymentDueOn;
				invoice.BeenPayedFor = beenPayedFor;
			};

			context.Invoices.Update(invoice);
			context.SaveChanges();

		}
		public static Invoice GenerateInvoice(Student student, Course course, CourseInstance instance)
		{
			// Payment is 4 weeks before the starting date.
			var dueDate = instance.StartDate.AddDays(-14);
			return Add(student, course, course.Price, dueDate);
		}

		public static Invoice Find(int invoiceId)
		{
			var context = new MyContext();
			try
			{
				return context.Invoices.First(i => i.Id == invoiceId);
			}
			catch (Exception e)
			{
				return null;
			}
		}

		public static ICollection<Invoice> FindExpired(int days = 14)
		{
			var context = new MyContext();
			return context.Invoices.Where(c=>c.PaymentDueOn < DateTime.Now.AddDays(days)).ToList();
		}

		public static ICollection<Invoice> FindBeenPayedFor(bool payedFor = true)
		{
			var context = new MyContext();
			return context.Invoices.Where(c => c.BeenPayedFor.Equals(payedFor)).ToList();
		}

		public Mail GenerateInvoiceMail(bool reminder = false)
		{
			var invoice = this;
			var subject = $"Factuur aanmelding cursus \"{invoice.Course.Name}\"";
			if (reminder) subject = $"Herinnering factuur aanmelding cursus \"{invoice.Course.Name}\"";
			var invoiceMail = new Mail(invoice.Student.EmailAddress, subject, true);
			invoiceMail.AddHeader($"Beste {invoice.Student.FullName},");
			if (!reminder) invoiceMail.AddParagraph($"Met genoegen bevestigen we hierbij jouw deelname aan de cursus \"{invoice.Course.Name}\".");
			if (reminder) invoiceMail.AddParagraph($"Graag verzoeken wij u om de openstaande factuur te voldoen voor cursus \"{invoice.Course.Name}\".");
			invoiceMail.AddBreak();
			invoiceMail.AddParagraph($"Graag verzoeken we je bij deze de betaling van &euro;{invoice.Amount} euro voor {invoice.PaymentDueOn.ToString("dd MMMM yyyy")} te voldoen aan de hand van de volgende gegevens:");
			invoiceMail.AddParagraph("IBAN: NL88KNAB0256484323");
			invoiceMail.AddParagraph("Ten name van: Disciplina");
			invoiceMail.AddParagraph($"Factuurnummer: {invoice.Id}");
			invoiceMail.AddParagraph("Onder vermelding van uw naam en de datum van de betreffende cursus.");
			invoiceMail.AddBreak();
			invoiceMail.AddParagraph("We wensen jou alvast veel succes op je cursus,");
			invoiceMail.AddBreak();
			invoiceMail.AddParagraph("Disciplina");
			return invoiceMail;
		}
	}
}
