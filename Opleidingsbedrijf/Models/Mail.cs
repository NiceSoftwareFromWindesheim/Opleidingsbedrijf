﻿using System;
using System.Configuration;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Windows;
using Microsoft.Extensions.Configuration;

namespace Opleidingsbedrijf.Models
{
    public class Mail
    {
        public SmtpClient SmtpServer { get; set; }
        public IConfiguration Settings { get; set; }
        public StringBuilder Content { get; set; }
        public string Recipient { get; set; }
        public string Subject { get; set; }
        public bool Template { get; set; }
        public Mail()
        { 
            Content = new StringBuilder();
            Settings = Config.settings.GetSection("SmtpSettings");
            SmtpServer = new SmtpClient( Settings.GetValue<string>("Server") );
            SmtpServer.Port = Settings.GetValue<int>("Port");
            SmtpServer.Credentials = new System.Net.NetworkCredential(
                Settings.GetValue<string>("Email"),
                Settings.GetValue<string>("Password")
                );
            SmtpServer.EnableSsl = Settings.GetValue<bool>("SSL");
        }

        public Mail(string recipient, string subject, bool template) : this()
        {
            Recipient = recipient;
            Subject = subject;
            Template = template;
        }

        public bool Send()
        {
            if (Template)
            { 
                GenerateTemplate();
            }
            return SendPlain();
        }

        private bool SendPlain()
        {
            try
            {
                MailMessage mail = new MailMessage();

                mail.From = new MailAddress(
                    Settings.GetValue<string>("Email")
                );
                mail.To.Add(Recipient);
                mail.Subject = Subject;
                mail.Body = Content.ToString();
                mail.IsBodyHtml = Template;
                SmtpServer.Send(mail);
                
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return false;
            }
        }

        public void GenerateTemplate()
        {
            var templateString = File.ReadAllText("assets/mailtemplate.html");
            var contentString = Content.ToString();

            templateString = templateString.Replace("{TITLE}", Subject);
            templateString = templateString.Replace("{CONTENT}", contentString);

            Content.Clear().Append(templateString);
        }
        public void AddHeader(string text)
        {
            if (Template)
            {
                Content.Append(
                    $"<h1 style=\"font-family: sans-serif; font-size: 28px; font-weight: bold; margin: 0; Margin-bottom: 30px;\">{text}</h1>\n");
            }
            else
            {
                Content.Append($"{text}\n");
            }
        }
        public void AddParagraph(string text)
        {
            if (Template)
            {
                Content.Append(
                    $"<p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">{text}</p>\n");
            }
            else
            {
                AddHeader(text);
            }
        }

        public void AddBreak()
        {
            if (Template)
            {
                Content.Append("<br>\n");
            }
            else
            {
                Content.Append("\n");
            }
        }
    }
}