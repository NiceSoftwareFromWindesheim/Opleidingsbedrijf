﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Opleidingsbedrijf.Models;

namespace UnitTest.Models
{
    public class InvoiceTest
    {
        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<MyContext>()
                .UseInMemoryDatabase(databaseName: "InvoiceTest")
                .Options;
            
            BaseModel.Context = new MyContext(options);
        }

        [Test]
        public void Add()
        {
            var student = "student";
            var course = "cursus";
            var amount = 100;
            var paymentDueOn = DateTime.Parse("2019/12/12");
            var beenPayedFor = DateTime.Parse("2019/11/12");

            var CreatedInvoice = Invoice.Add(student, course, amount, paymentDueOn, beenPayedFor);
            
            
            Assert.AreEqual(BaseModel.Context.Invoices.Count(), 1);
            Assert.AreEqual(CreatedInvoice.Student, student);
            Assert.AreEqual(CreatedInvoice.Course, course);
            Assert.AreEqual(CreatedInvoice.Amount, amount);
            Assert.AreEqual(CreatedInvoice.PaymentDueOn, paymentDueOn);
            Assert.AreEqual(CreatedInvoice.BeenPayedFor, beenPayedFor);
        }

        [Test]
        public void Update()
        {
            var student = "student";
            var course = "cursus";
            var amount = 110;
            var paymentDueOn = DateTime.Parse("2020/12/12");
            var beenPayedFor = DateTime.Parse("2020/11/12");

            var UpdatedInvoice = Invoice.Update(1, amount, paymentDueOn, beenPayedFor);
            
            Assert.AreEqual(BaseModel.Context.Invoices.Count(), 1);
            Assert.AreEqual(UpdatedInvoice.Amount, amount);
            Assert.AreEqual(UpdatedInvoice.PaymentDueOn, paymentDueOn);
            Assert.AreEqual(UpdatedInvoice.BeenPayedFor, beenPayedFor);
        }
        
    }
}