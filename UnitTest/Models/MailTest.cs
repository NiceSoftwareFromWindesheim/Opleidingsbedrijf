﻿using NUnit.Framework;
using Opleidingsbedrijf.Models;

namespace UnitTest.Models
{
    public class MailTest
    {
        private Mail mail;
        
        [SetUp]
        public void Setup()
        {
            //Arrange
            mail = new Mail("example@example.com", "Testmail", true);
        }

        [Test]
        public void TestAddHeader()
        {
            // Act
            mail.AddHeader("TestHeader");
            
            // Assert
            Assert.AreEqual(
                "<h1 style=\"font-family: sans-serif; font-size: 28px; font-weight: bold; margin: 0; Margin-bottom: 30px;\">TestHeader</h1>\n",
                mail.Content.ToString()
            );
        }
        
        [Test]
        public void TestAddParagraph()
        {
            // Act
            mail.AddParagraph("Lorem augue ad vulputate nisl ullamcorper quam parturient consectetur taciti senectus primis non enim parturient penatibus felis aliquam cursus sodales ipsum.");
            mail.AddParagraph("Egestas curabitur mi condimentum ad nec interdum habitant molestie a elit ultricies laoreet ante a condimentum suspendisse suspendisse ridiculus.");
            
            // Assert
            Assert.AreEqual(
                "<p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">Lorem augue ad vulputate nisl ullamcorper quam parturient consectetur taciti senectus primis non enim parturient penatibus felis aliquam cursus sodales ipsum.</p>\n<p style=\"font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;\">Egestas curabitur mi condimentum ad nec interdum habitant molestie a elit ultricies laoreet ante a condimentum suspendisse suspendisse ridiculus.</p>\n",
                mail.Content.ToString()
            );
        }
        
        [Test]
        public void TestAddBreak()
        {
            // Act
            mail.AddBreak();            
            mail.AddBreak();            
            mail.AddBreak();
            
            // Assert
            Assert.AreEqual(
                "<br>\n<br>\n<br>\n",
                mail.Content.ToString()
            );
        }
    }
}