﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Opleidingsbedrijf.Models;


namespace UnitTest.Models
{
    public class CourseTest
    {
        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<MyContext>()
                .UseInMemoryDatabase(databaseName: "CourseTest")
                .Options;

            BaseModel.Context = new MyContext(options);
        }

        [Test]
        public void Add()
        {
            var name = "name";
            var description = "description";
            var price = 100;
            var maxStudentCount = 25;
            var requirements = " ";

            var CreatedCourse = Course.Add(name, description, price, maxStudentCount, requirements);

            Assert.AreEqual(BaseModel.Context.Courses.Count(), 1);
            Assert.AreEqual(CreatedCourse.Name, name);
            Assert.AreEqual(CreatedCourse.Description, description);
            Assert.AreEqual(CreatedCourse.Price, price);
            Assert.AreEqual(CreatedCourse.MaxStudentCount, maxStudentCount);
            Assert.AreEqual(CreatedCourse.Requirements, requirements);
        }

        [Test]
        public void Update()
        {
            var name = "Newname";
            var description = "Newdescription";
            var price = 75;
            var maxStudentCount = 35;
            var requirements = " ";

            var UpdatedCourse = Course.Update(1,name, price, description, maxStudentCount, requirements);

            Assert.AreEqual(BaseModel.Context.Courses.Count(), 1);
            Assert.AreEqual(UpdatedCourse.Name, name);
            Assert.AreEqual(UpdatedCourse.Description, description);
            Assert.AreEqual(UpdatedCourse.Price, price);
            Assert.AreEqual(UpdatedCourse.MaxStudentCount, maxStudentCount);
            Assert.AreEqual(UpdatedCourse.Requirements, requirements);
        }
    }
}
