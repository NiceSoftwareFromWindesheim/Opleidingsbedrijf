﻿using System;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Opleidingsbedrijf.Models;

namespace UnitTest.Models
{
    public class StudentTest
    {
        [SetUp]
        public void Setup()
        {
            var options = new DbContextOptionsBuilder<MyContext>()
                .UseInMemoryDatabase(databaseName: "StudentTest")
                .Options;
            
            BaseModel.Context = new MyContext(options);
        }
        
        [Test]
        public void Add()
        {
            var FirstName = "firstName";
            var LastName = "lastName";
            var DateOfBirth = DateTime.Parse("2019/2/2");
            var EmailAddress = "email@address.com";
            var Address = "address";
            var PostalCode = "postalCode";
            var HouseNumber = 1;
            var HouseNumberAddition = "AA";
            var City = "city";
            var PhoneNumber = "+316000000";
            
            var CreatedStudent = Student.Add(FirstName, LastName, DateOfBirth, EmailAddress, Address,
                PostalCode, HouseNumber, HouseNumberAddition, City, PhoneNumber);
            
            Assert.AreEqual(BaseModel.Context.Students.Count(), 1);
            Assert.AreEqual(CreatedStudent.FirstName, FirstName);
            Assert.AreEqual(CreatedStudent.LastName, LastName);
            Assert.AreEqual(CreatedStudent.DateOfBirth, DateOfBirth);
            Assert.AreEqual(CreatedStudent.EmailAddress, EmailAddress);
            Assert.AreEqual(CreatedStudent.PostalCode, PostalCode);
            Assert.AreEqual(CreatedStudent.HouseNumber, HouseNumber);
            Assert.AreEqual(CreatedStudent.HouseNumberAddition, HouseNumberAddition);
            Assert.AreEqual(CreatedStudent.City, City);
            Assert.AreEqual(CreatedStudent.PhoneNumber, PhoneNumber);
        }
        
        [Test]
        public void Update()
        {
            var FirstName = "newFirstName";
            var LastName = "newLastName";
            var DateOfBirth = DateTime.Parse("2020/2/2");
            var EmailAddress = "newEmail@address.com";
            var Address = "newAddress";
            var PostalCode = "newPostalCode";
            var HouseNumber = 2;
            var HouseNumberAddition = "BB";
            var City = "newCity";
            var PhoneNumber = "+316000001";
            
            var UpdatedStudent = Student.Update(1, FirstName, LastName, DateOfBirth, EmailAddress, Address,
                PostalCode, HouseNumber, HouseNumberAddition, City, PhoneNumber);
            
            Assert.AreEqual(BaseModel.Context.Students.Count(), 1);
            Assert.AreEqual(UpdatedStudent.FirstName, FirstName);
            Assert.AreEqual(UpdatedStudent.LastName, LastName);
            Assert.AreEqual(UpdatedStudent.DateOfBirth, DateOfBirth);
            Assert.AreEqual(UpdatedStudent.EmailAddress, EmailAddress);
            Assert.AreEqual(UpdatedStudent.PostalCode, PostalCode);
            Assert.AreEqual(UpdatedStudent.HouseNumber, HouseNumber);
            Assert.AreEqual(UpdatedStudent.HouseNumberAddition, HouseNumberAddition);
            Assert.AreEqual(UpdatedStudent.City, City);
            Assert.AreEqual(UpdatedStudent.PhoneNumber, PhoneNumber);
        }
    }
}