﻿using System;
using NUnit.Framework;
using Opleidingsbedrijf.Models;
using Opleidingsbedrijf.ViewModels;

namespace UnitTest.ViewModels
{
    public class PresenterTest
    {
        [Test]
        public void StudentPresenterPageSwitch()
        {
            // Arrange
            var StudentPresenter = new StudentPresenter();

            bool eventThrown = false;
            StudentPresenter.PropertyChanged += (o,e) => eventThrown = true;
            
            // Act
            var result1 = StudentPresenter.SelectedContent;

            StudentPresenter.ExecuteAddLoadContent();
            var result2 = StudentPresenter.SelectedContent;
            
            StudentPresenter.ExecuteAddLoadContent();
            var result3 = StudentPresenter.SelectedContent;

            StudentPresenter.ExecuteLoadEditContent();
            var result4 = StudentPresenter.SelectedContent;

            StudentPresenter.ExecuteLoadEditContent();
            var result5 = StudentPresenter.SelectedContent;
            
            // Assert
            Assert.IsInstanceOf<StudentOverviewModel>(result1);
            Assert.IsInstanceOf<StudentAddModel>(result2);
            Assert.IsInstanceOf<StudentOverviewModel>(result3);
            Assert.IsInstanceOf<StudentManageModel>(result4);
            Assert.IsInstanceOf<StudentOverviewModel>(result5);
            
            Assert.IsTrue(eventThrown);
        }
        
        [Test]
        public void CoursePresenterPageSwitch()
        {
            // Arrange
            var CoursePresenter = new CoursePresenter();

            bool eventThrown = false;
            CoursePresenter.PropertyChanged += (o,e) => eventThrown = true;

            // Act
            var result1 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadContent();
            var result2 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadContent();
            var result3 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadDataContent();
            var result4 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadDataContent();
            var result5 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadAddContent();
            var result6 = CoursePresenter.SelectedContent;

            CoursePresenter.ExecuteLoadAddContent();
            var result7 = CoursePresenter.SelectedContent;
            
            // Assert
            Assert.IsInstanceOf<CourseOverviewModel>(result1);
            Assert.IsInstanceOf<CourseManageModel>(result2);
            Assert.IsInstanceOf<CourseOverviewModel>(result3);
            Assert.IsInstanceOf<CourseDataModel>(result4);
            Assert.IsInstanceOf<CourseOverviewModel>(result5);
            Assert.IsInstanceOf<CourseAddModel>(result6);
            Assert.IsInstanceOf<CourseOverviewModel>(result7);
            
            Assert.IsTrue(eventThrown);
        }
        
        [Test]
        public void AddCoursePresenterPageSwitch()
        {
            // Arrange
            var AddCoursePresenter = new AddCourseMomentPresenter();

            bool eventThrown = false;
            AddCoursePresenter.PropertyChanged += (o,e) => eventThrown = true;
            
            // Act
            var result1 = AddCoursePresenter.SelectedContent;

            AddCoursePresenter.ExecuteLoadContent();
            var result2 = AddCoursePresenter.SelectedContent;

            AddCoursePresenter.ExecuteLoadContent();
            var result3 = AddCoursePresenter.SelectedContent;
            
            // Assert
            Assert.IsInstanceOf<CourseDataModel>(result1);
            Assert.IsInstanceOf<AddCourseMomentModel>(result2);
            Assert.IsInstanceOf<CourseDataModel>(result3);

            Assert.IsTrue(eventThrown);
        }
    }
}